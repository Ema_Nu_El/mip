package sample.data;

import sample.EntityManagerProvider;
import sample.models.Appointment;

import java.util.List;

public class AppointmentDao {

    public List<Appointment> getAppointments() {
        return EntityManagerProvider.getInstance()
                .createNativeQuery("SELECT * FROM appointment", Appointment.class)
                .getResultList();
    }
}
