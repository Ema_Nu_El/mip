package sample;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerProvider {
    private static EntityManager instance;

    public static EntityManager getInstance() {
        if (instance == null) {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("VetCabinet");
            instance = factory.createEntityManager();
        }
        return instance;
    }
}
