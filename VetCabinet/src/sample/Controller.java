package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import sample.data.AppointmentDao;
import sample.models.Appointment;

import javax.persistence.EntityManager;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private ListView<Appointment> appointmentsListView;

    @FXML
    private Label appointmentNameLabel;

    private AppointmentDao appointmentDao = new AppointmentDao();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        EntityManager manager = EntityManagerProvider.getInstance();

        List<Appointment> appointments = appointmentDao.getAppointments();

        appointmentsListView.setItems(FXCollections.observableArrayList(appointments));

        appointmentsListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            appointmentNameLabel.setText(observable.getValue().toString());
        });

    }
}
