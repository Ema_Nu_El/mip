package sample.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Appointment {
    private int idAppointment;
    private Timestamp date;
    private Integer idAnimal;
    private Integer idPersonnel;

    @Id
    @Column(name = "idAppointment", nullable = false)
    public int getIdAppointment() {
        return idAppointment;
    }

    public void setIdAppointment(int idAppointment) {
        this.idAppointment = idAppointment;
    }

    @Basic
    @Column(name = "date", nullable = true)
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "idAnimal", nullable = true)
    public Integer getIdAnimal() {
        return idAnimal;
    }

    public void setIdAnimal(Integer idAnimal) {
        this.idAnimal = idAnimal;
    }

    @Basic
    @Column(name = "idPersonnel", nullable = true)
    public Integer getIdPersonnel() {
        return idPersonnel;
    }

    public void setIdPersonnel(Integer idPersonnel) {
        this.idPersonnel = idPersonnel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Appointment that = (Appointment) o;
        return idAppointment == that.idAppointment &&
                Objects.equals(date, that.date) &&
                Objects.equals(idAnimal, that.idAnimal) &&
                Objects.equals(idPersonnel, that.idPersonnel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAppointment, date, idAnimal, idPersonnel);
    }

    @Override
    public String toString() {
        return this.date.toString();
    }
}
