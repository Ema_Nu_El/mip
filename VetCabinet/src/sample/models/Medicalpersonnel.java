package sample.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Medicalpersonnel {
    private int idMedicalPersonnel;
    private String name;

    @Id
    @Column(name = "idMedicalPersonnel", nullable = false)
    public int getIdMedicalPersonnel() {
        return idMedicalPersonnel;
    }

    public void setIdMedicalPersonnel(int idMedicalPersonnel) {
        this.idMedicalPersonnel = idMedicalPersonnel;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medicalpersonnel that = (Medicalpersonnel) o;
        return idMedicalPersonnel == that.idMedicalPersonnel &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMedicalPersonnel, name);
    }
}
